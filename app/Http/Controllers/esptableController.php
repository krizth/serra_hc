<?php

namespace App\Http\Controllers;

use App\Models\Esptable2;
use Illuminate\Http\Request;

class esptableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Esptable2::create([

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $q= Esptable2::where([
            ['id',"=",$request->id],
            ['password',"=",$request->pw]
        ]);
       switch ($request->update_number){
           case 1:
               $q->update([
                    'SENT_NUMBER_1'=>$request->n1
               ]);
               break;
           case 2:
               $q->update([
                   'SENT_NUMBER_2'=>$request->n2
               ]);
               break;
           case 3:
               $q->update([
                   'SENT_NUMBER_1'=>$request->n3
               ]);
               break;
           case 4:
               $q->update([
                   'SENT_NUMBER_1'=>$request->n4
               ]);
           case 5:
               $q->update([
                   'SENT_BOOL_1'=>$request->b6,
                   'SENT_BOOL_2'=>$request->b7,
                   'SENT_BOOL_3'=>$request->b8,
               ]);
       }


       $resp=Esptable2::find($request->id);
       $t1=date("gi");

       return " _t1$t1##_b1$resp->RECEIVED_BOOL1##_b2$resp->RECEIVED_BOOL2##_b3$resp->RECEIVED_BOOL3##_b4$resp->RECEIVED_BOOL4##_b5$resp->RECEIVED_BOOL5##_n1$resp->RECEIVED_NUM1##_n2$resp->RECEIVED_NUM2##_n3$resp->RECEIVED_NUM3##_n4$resp->RECEIVED_NUM4##_n5$resp->RECEIVED_NUM5##_n6$resp->TEXT_1##";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
