<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $PASSWORD
 * @property int $SENT_NUMBER_1
 * @property int $SENT_NUMBER_2
 * @property int $SENT_NUMBER_3
 * @property int $SENT_NUMBER_4
 * @property boolean $SENT_BOOL_1
 * @property boolean $SENT_BOOL_2
 * @property boolean $SENT_BOOL_3
 * @property int $RECEIVED_BOOL1
 * @property int $RECEIVED_BOOL2
 * @property int $RECEIVED_BOOL3
 * @property int $RECEIVED_BOOL4
 * @property int $RECEIVED_BOOL5
 * @property int $RECEIVED_NUM1
 * @property int $RECEIVED_NUM2
 * @property int $RECEIVED_NUM3
 * @property int $RECEIVED_NUM4
 * @property int $RECEIVED_NUM5
 * @property string $TEXT_1
 */
class Esptable2 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ESPtable2';

    /**
     * @var array
     */
    protected $fillable = ['id', 'PASSWORD', 'SENT_NUMBER_1', 'SENT_NUMBER_2', 'SENT_NUMBER_3', 'SENT_NUMBER_4', 'SENT_BOOL_1', 'SENT_BOOL_2', 'SENT_BOOL_3', 'RECEIVED_BOOL1', 'RECEIVED_BOOL2', 'RECEIVED_BOOL3', 'RECEIVED_BOOL4', 'RECEIVED_BOOL5', 'RECEIVED_NUM1', 'RECEIVED_NUM2', 'RECEIVED_NUM3', 'RECEIVED_NUM4', 'RECEIVED_NUM5', 'TEXT_1'];

}
